class BST
  class Node < Struct.new(:data, :left, :right)
    alias_method :show, :data
    def count
      @count ||= 1
    end

    def count=(num)
      @count = num
    end
  end
end

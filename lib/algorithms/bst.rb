require 'ostruct'
require 'algorithms/bst/node'
# Binary Search Tree
class BST
  attr_reader :root
  def insert(data)
    node = Node.new(data, nil, nil)
    if root.nil?
      @root = node
    else
      current = root
      parent = nil
      loop do
        parent = current
        if data < current.data
          current = current.left
          if current.nil?
            parent.left = node
            break
          end
        else
          current = current.right
          if current == nil
            parent.right = node
            break
          end
        end
      end
    end
  end

  def in_order(node, &block)
    return unless node
    in_order(node.left, &block)
    yield node.show if block_given?
    in_order(node.right, &block)
  end

  def pre_order(node, &block)
    return unless node
    yield node.show if block_given?
    in_order(node.left, &block)
    in_order(node.right, &block)
  end

  def post_order(node, &block)
    return unless node
    in_order(node.left, &block)
    in_order(node.right, &block)
    yield node.show if block_given?
  end

  def get_min(node = nil)
    current = node || root
    loop do
      return current if current.left.nil?
      current = current.left
    end
  end
  alias_method :get_smallest, :get_min

  def get_max(node = nil)
    current = node || root
    loop do
      return current if current.right.nil?
      current = current.right
    end
  end
  alias_method :get_biggest, :get_max

  def find(data)
    current = root
    return unless root
    while current.data != data
      current = if data < current.data
                  current.left
                else
                  current.right
                end
      return if current.nil?
    end
    current
  end

  def remove(data)
    remove_node(root, data)
  end

  def update(data)
    grade = find(data)
    return unless grade
    grade.tap{|g| g.count += 1 }
  end

  def update_or_insert(data)
    if found = find(data)
      found.tap{|g| g.count += 1 }
    else
      insert(data)
    end
  end

  def self.from_a(*ary)
    elems = ary.flatten
    self.new.tap do |bst|
      elems.each do |elem|
        bst.update_or_insert(elem)
      end
    end
  end

  private

  def remove_node(node, data)
    return if node.nil?
    if data == node.data
      # return the descendant node if
      # node has no children
      return if node.left.nil? && node.right.nil?
      # node has no right child
      return node.right if node.left.nil?
      # node has no left child
      return node.left if node.right.nil?
      # node has two children
      temp_node = get_min(node.right)
      node.data = temp_node.data
      node.right = remove_node(node.right, temp_node.data)
      return node
    elsif data < node.data
      node.left = remove_node(node.left, data)
      return node
    else
      node.right = remove_node(node.right, data)
      return node
    end
  end

end

require 'forwardable'
module Algorithms
  class DepthFirstSearch
    extend Forwardable
    def_delegator :graph, :adjacents
    attr_reader :graph, :visited


    def initialize(graph)
      @graph = graph
      @visited = []
    end

    def dfs(v)
      puts v
      visited << v
      if adjacents[v] != nil
        puts "Visited vertex #{v}"
        adjacents[v].each do |elem|
          unless visited.include? elem
            dfs(elem)
          end
        end
      end
    end
  end
end

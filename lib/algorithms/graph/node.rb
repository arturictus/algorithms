module Algorithms
  class Graph
    class Node
      attr_reader :value, :adjacents
      def initialize(value)
        @value = value
        @adjacents = []
      end

      def <<(value)
        adjacents << value
      end

      def count
        adjacents.count
      end
    end
  end
end

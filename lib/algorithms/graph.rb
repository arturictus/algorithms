class Graph
  class Vertex < Struct.new(:label, :was_visited); end
  attr_reader :vertices, :adjacents, :edges
  def initialize(vertices)
    @vertices = vertices
    @edges = 0
    @adjacents = []
    each_vertex do |i|
      adjacents[i] = []
    end
  end

  def add_edge(v, w)
    adjacents[v] << w
    adjacents[w] << v
    @edges += 1
  end

  def to_s
    str = ''
    each_vertex do |i|
      str += "#{i} -> "
      each_vertex do |j|
        str += "#{adj} " if (adj = adjacents[i][j])
      end
    end
  end

  def to_h
    hash = {}
    each_vertex do |i|
      hash[i] = []
      each_vertex do |j|
        if adj = adjacents[i][j]
          hash[i] << adj
        end
      end
    end
    hash
  end

  def show_graph
    puts to_s
  end


  def each_vertex
    i = 0
    while i < vertices
      yield i
      i += 1
    end
  end
end

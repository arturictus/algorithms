require 'spec_helper'
class BST
  describe Node do
    subject { BST::Node.new('data', 'left', 'right') }
    it { expect(subject.data).to eq 'data' }
    it { expect(subject.left).to eq 'left' }
    it { expect(subject.right).to eq 'right' }
    it { expect(subject.show).to eq 'data' }
    it { expect(subject.count).to eq 1 }
  end
end

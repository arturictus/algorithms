require 'spec_helper'
module Algorithms
  describe DepthFirstSearch do
    let(:graph) do
      described_class.new(5).tap do |g|
        g.add_edge(0,1)
        g.add_edge(0,2)
        g.add_edge(1,3)
        g.add_edge(2,4)
      end
    end
    subject do
      described_class.new(graph)
    end

    describe '#find' do
      it do
        expect(subject.search(4)).to eq nil
      end
    end

  end
end

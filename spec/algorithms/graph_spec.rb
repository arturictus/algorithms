require 'spec_helper'
require 'byebug'
module Algorithms
  describe Graph do
    subject(:graph) do
      described_class.new(5).tap do |g|
        g.add_edge(0,1)
        g.add_edge(0,2)
        g.add_edge(1,3)
        g.add_edge(2,4)
      end
    end

    it ('vertices'){ expect(subject.vertices).to eq 5 }
    it 'adjacents.count' do
      expect(subject.adjacents.count).to eq 5
    end
    it ('#to_s includes') do
      expect(subject.to_s).to include('0 -> 1 2')
    end

    describe '#to_h' do
      subject { graph.to_h }
      it { expect(subject[0]).to match_array [1, 2] }
      it { expect(subject[1]).to match_array [0, 3] }
      it { expect(subject[2]).to match_array [0, 4] }
      it { expect(subject[3]).to match_array [1] }
      it { expect(subject[4]).to match_array [2] }
    end
  end
end

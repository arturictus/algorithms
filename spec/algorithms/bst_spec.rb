require 'spec_helper'
describe BST do
  subject!(:random_numbers) do
    BST.new.tap do |o|
      o.insert(23)
      o.insert(45)
      o.insert(16)
      o.insert(37)
      o.insert(3)
      o.insert(99)
      o.insert(22)
    end
  end
  describe '::from_a' do
    let(:bst){ BST.from_a(1, 2, 3, 4, 5) }
    it { expect(bst.find(5).data).to eq 5 }
    it { expect(bst.find(2).data).to eq 2 }
    it { expect(bst.find(3).data).to eq 3 }
    it { expect(bst.find(3).left.data).to eq 2 }
  end
  describe '#insert' do
    let!(:bst) { BST.new }
    it do
      expect do
        bst.insert('data')
      end.to change(bst, :root).from(nil).to(instance_of(BST::Node))
    end
    describe 'grouping numerically' do
      subject! { BST.new.tap{|o| o.insert(5) } }
      let(:insert_4) { subject.insert(4) }
      let(:insert_6) { subject.insert(6) }
      it do
        expect do
          insert_4
        end.to change(subject.root, :left).from(nil).to(instance_of(BST::Node))
      end
      it do
        insert_4
        expect(subject.root.left.data).to eq 4
      end
      it do
        expect do
          insert_6
        end.to change(subject.root, :right).from(nil).to(instance_of(BST::Node))
      end
      it do
        insert_6
        expect(subject.root.right.data).to eq 6
      end
    end
  end
  describe '#in_order' do
    it do
      ary = []
      subject.in_order(subject.root) { |o| ary << o }
      expect(ary).to eq [3, 16, 22, 23, 37, 45, 99]
    end
  end
  describe '#pre_order' do
    it do
      ary = []
      subject.pre_order(subject.root) { |o| ary << o }
      expect(ary).to eq [23, 16, 3, 22, 45, 37, 99]
    end
  end
  describe '#post_order' do
    it do
      ary = []
      subject.post_order(subject.root) { |o| ary << o }
      expect(ary).to eq [3, 22, 16, 37, 99, 45, 23]
    end
  end
  describe '#get_min' do
    it { expect(subject.get_min.data).to eq 3 }
  end
  describe '#get_max' do
    it { expect(subject.get_max.data).to eq 99 }
  end
  describe '#find' do
    it { expect(subject.find(3)).to be_a(BST::Node) }
    it { expect(subject.find(3).data).to eq(3) }
    it { expect(subject.find(66)).to be_nil }
  end
  describe '#remove' do
    context 'node without nodes' do
      let!(:bst) do
        BST.new.tap do |o|
          o.insert(23)
          o.insert(3)
        end
      end
      let(:remove_3) { bst.remove(3) }
      context 'output' do
        it { expect(remove_3.left).to be_nil }
        it { expect(remove_3.right).to be_nil }
        it { expect(remove_3.data).to eq 23 }
      end
      context 'after remove 3' do
        before { remove_3 }
        it { expect(bst.find(3)).to be_nil }
        it { expect(bst.root.data).to eq 23 }
        it { expect(bst.root.left).to be_nil }
        it { expect(bst.root.right).to be_nil }
      end
    end
    context 'node with left node and without right node' do
      let!(:bst) do
        BST.new.tap do |o|
          o.insert(23)
          o.insert(3)
          o.insert(4)
        end
      end
      let(:remove_3) { bst.remove(3) }
      context 'output' do
        it { expect(remove_3.left.data).to eq 4 }
        it { expect(remove_3.right).to be_nil }
        it { expect(remove_3.data).to eq 23 }
      end
      context 'after remove 3' do
        before { remove_3 }
        it { expect(bst.find(3)).to be_nil }
        it { expect(bst.root.left.data).to eq(4) }
      end
    end
    context 'with left and right' do
      let(:remove_45) { subject.remove(45) }
      context 'output' do
        it { expect(remove_45.data).to eq 23 }
      end
      context 'after remove 37' do
        before { remove_45 }
        it { expect(subject.find(45)).to be_nil }
        it { expect(subject.find(99).left.data).to eq 37 }
        it { expect(subject.find(23).right.data).to eq 99 }
        it { expect(subject.find(37).right).to be_nil }
        it { expect(subject.find(37).left).to be_nil }
      end
    end
  end
  describe '#update' do
    it { expect{ subject.update(3) }.to change(subject.find(3), :count).by 1 }
  end
  describe '#update_or_insert' do
    it('updating') { expect{ subject.update_or_insert(3) }.to change(subject.find(3), :count).by 1 }
    it('inserting') do
      subject.update_or_insert(67)
      expect(subject.find(67).data).to eq(67)
    end
  end
end

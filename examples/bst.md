Binary search Tree
==================
initializing:
```ruby
bst = BST.new.tap do |o|
  o.insert(23)
  o.insert(45)
  o.insert(16)
  o.insert(37)
  o.insert(3)
  o.insert(99)
  o.insert(22)
end
```
# contruction:
```ruby
y bst.root
--- !ruby/struct:BST::Node
data: 23
left: !ruby/struct:BST::Node
  data: 16
  left: !ruby/struct:BST::Node
    data: 3
    left:
    right:
  right: !ruby/struct:BST::Node
    data: 22
    left:
    right:
right: !ruby/struct:BST::Node
  data: 45
  left: !ruby/struct:BST::Node
    data: 37
    left:
    right:
  right: !ruby/struct:BST::Node
    data: 99
    left:
    right:
```
## remove:
```ruby
y bst.remove(45) # returns the parent node
--- !ruby/struct:BST::Node
data: 23
left: !ruby/struct:BST::Node
  data: 16
  left: !ruby/struct:BST::Node
    data: 3
    left:
    right:
  right: !ruby/struct:BST::Node
    data: 22
    left:
    right:
right: !ruby/struct:BST::Node
  data: 99
  left: !ruby/struct:BST::Node
    data: 37
    left:
    right:
  right:
```
```ruby
y bst.root
--- !ruby/struct:BST::Node
data: 23
left: !ruby/struct:BST::Node
  data: 16
  left: !ruby/struct:BST::Node
    data: 3
    left:
    right:
  right: !ruby/struct:BST::Node
    data: 22
    left:
    right:
right: !ruby/struct:BST::Node
  data: 99
  left: !ruby/struct:BST::Node
    data: 37
    left:
    right:
  right:
```
